# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



users = User.create([
  { email: 'yoosuf@3prg.com', password: 'password' }, 
  { email: 'phill@3prg.com', password: 'password' }, 
  { email: 'ahesh@3prg.com', password: 'password' }, 
  { email: 'sagara@3prg.com', password: 'password' }
  ])