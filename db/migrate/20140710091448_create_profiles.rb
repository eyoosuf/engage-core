class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.date :date_of_birth
      t.string :job_title
      t.text :bio
      t.string :pic_url
      t.string :pic_url_thumb
      t.string :pic_url_medium
      t.references :user
      t.references :account

      t.timestamps
    end
    add_index :profiles, :user_id
    add_index :profiles, :account_id
  end
end
