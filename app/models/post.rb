class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :account
  attr_accessible :content
  has_many :comments, :as => :commentable
  
  
  validates :content, :presence => true
  
  before_save do
    self.content.strip! unless self.content.nil?
  end

  after_save do
    self.touch
  end
  
end
