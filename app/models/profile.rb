class Profile < ActiveRecord::Base
  attr_accessible :bio, :date_of_birth, :first_name, :job_title, :last_name, :pic_url, :pic_url_medium, :pic_url_thumb
  
  mount_uploader :pic_url, PhotoUploader
   
  belongs_to :user
  belongs_to :account
  


  validates :first_name, :presence => true
  validates :last_name, :presence => true


end
