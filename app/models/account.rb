class Account < ActiveRecord::Base
  attr_accessible :name
  
  has_many :users
  has_many :accounts
  has_many :posts
  
  has_many :comments
  
end
