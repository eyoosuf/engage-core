class User < ActiveRecord::Base
  
  has_one :profile
  belongs_to :account
  
  accepts_nested_attributes_for :profile
  has_many :posts
  

  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,:profile_attributes
  # attr_accessible :title, :body
  
  
  def profile
    super || build_profile
  end
  
end
